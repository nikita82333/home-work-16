﻿
#include <iostream>
#include <ctime>
#include <iomanip>

int main()
{
    const int N = 10;
    int array[N][N];
    
    std::cout << "1. Matrix initialization..." << std::endl;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            array[i][j] = i + j;
        }
    }

    std::cout << "2. Printing the matrix to the screen:" << std::endl;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << std::setw(3) << array[i][j];
        }
        std::cout << std::endl;
    }

    //getting the current day
    std::time_t seconds = time(NULL);
    std::tm timeinfo;
    localtime_s(&timeinfo , &seconds);
    int day = timeinfo.tm_mday;

    std::cout << "3. Calculating the sum of the elements in the array row" << std::endl <<
                 " whose index is equal to the remainder of the division" << std::endl <<
                 " of the current calendar number by N." << std::endl;

    std::cout << "Current day: " << day << std::endl;
    int rowNumber = day % N;
    int sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += array[rowNumber][i];
    }
    std::cout << "Sum of elements of " << rowNumber << "-th row: " << sum << std::endl;

    return 0;
}
